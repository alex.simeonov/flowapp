# <img src="./documentation/assets/logo.png" width=30px /> Flow - Project Planning System

A project planning system made using Angular by Alexander Simeonov & Bogomil Valev.

## Local test

In order to run our project locally, you can clone this repository.

To install all dependencies run `npm i` in `flow-app` and `functions` folders.

Run `ng serve` and navigate to `http://localhost:4200/`.

Flow uses <a href="https://firebase.google.com/">Google Firebase</a>.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.5.

### Account credentials to sign in:
email: developer@dev.dev
<br>
password: developer

### Application bugs:
+ Filter by keyword - if we write in the input field, then we add skill filter or subordinates filter - the keyword filter is being ignored.
+ Concurrent editing and creating projects - If you assign the same employee to projects from different tabs at the same time - it's availability may become < 0.
+ Skill completes with full employee day hours.
+ Progress update(от Cloud Functions) - frees employee availability on the next day.

## Project overview

### Used packages:
- @angular/fire
- ng-bootstrap
- angular2-multiselect-dropdown

Flow is a company platform, and in order to sign in you need to be a manager.


<img src="./documentation/assets/login.png" />

After signing in managers have access to the dashboard, projects, employees pages and the skills page if the user is Admin. In the app you as a manager can create new projects or edit your existing ones. As admin you can add new employees or skills, and edit already existing ones.

Dashboard
<img src="./documentation/assets/dashboard.png" />
<hr>

Projects
<br>
<img src="./documentation/assets/projects.png" />

Create Project
<br>
<img src="./documentation/assets/create-project.png" />

Single Project View
<br>
<img src="./documentation/assets/project-page.png" />

Edit Project
<br>
<img src="./documentation/assets/edit-project.png" />
<hr>

Employees
<br>
<img src="./documentation/assets/employees.png" />

Create Employee
<br>
<img src="./documentation/assets/create-employee.png" />

Edit Employee
<br>
<img src="./documentation/assets/edit-employee.png" />

Single Employee View
<br>
<img src="./documentation/assets/single-employee.png" />

<hr>
Skills
<br>
<img src="./documentation/assets/skills.png" />

Create Skill
<br>
<img src="./documentation/assets/create-skill.png" />
