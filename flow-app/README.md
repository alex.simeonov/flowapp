# <img src="../documentation/assets/logo.png" width=30px /> Flow - Project Planning System

## Tick42 Final Project Assignment

A project planning system made using Angular by Alexander Simeonov & Bogomil Valev.

You can access [the documentation](../readme.md) here.
