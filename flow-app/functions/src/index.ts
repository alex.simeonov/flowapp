import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

export const createUser = functions
  .region('europe-west3')
  .https.onCall(async (data, context) => {
    return await admin.auth().createUser({
      email: data.email,
      password: data.password,
      displayName: data.displayName,
      photoURL: data.photoURL,
    });
  });

// firebase deploy --only functions:updateProjectProgress
export const updateProjectProgress = functions
  .region('europe-west2')
  .pubsub
  .schedule('0 0 * * 1-5') // execute every weekday
  .timeZone('Europe/Sofia')
  .onRun(async (context): Promise<null> => {

    const projectsSnapshot: admin.firestore.QuerySnapshot = await admin.firestore().collection('projects').get();
    const batch: admin.firestore.WriteBatch = admin.firestore().batch();

    if (!projectsSnapshot.empty) {
      // all projects forEach
      for (const projSnapshot of projectsSnapshot.docs) {
        // get all skill_employee and skills documents
        const skillsEmployeesSnapshot = await projSnapshot.ref.collection('skill_employee').get();
        const skillsSnapshot = await projSnapshot.ref.collection('skills').get();

        // if project is ongoing and has employees assigned
        if (!projSnapshot.get('progressStatus') && !skillsEmployeesSnapshot.empty) {
          batch.update(projSnapshot.ref, 'elapsedDays', admin.firestore.FieldValue.increment(1));
          // all project skills forEach
          for (const skillSnapshot of skillsSnapshot.docs) {
            // if skill is not completed yet, progress it
            if (skillSnapshot.get('progress') <= skillSnapshot.get('time')) {
              let skillTime = 0;

              // accumulate employee time for skill
              for (const skEmpSnapshot of skillsEmployeesSnapshot.docs) {
                if (skEmpSnapshot.id.includes(skillSnapshot.id)) {
                  skillTime += skEmpSnapshot.get('time');
                }
              }

              // update skill progress time
              batch.update(skillSnapshot.ref, 'progress', admin.firestore.FieldValue.increment(skillTime));
            } else if (skillSnapshot.get('employeeIds').length > 0) {
              // if skill is completed, update employees availabililty
              for (const empId of skillSnapshot.get('employeeIds')) {
                // skillSnapshot.get('employeeIds').forEach(async (empId: string) => {
                const skillId = skillSnapshot.get('id');

                const skillEmp: FirebaseFirestore.DocumentData | undefined = skillsEmployeesSnapshot.docs
                  .find((skillEmpDoc: admin.firestore.QueryDocumentSnapshot) => skillEmpDoc.id === skillId + '_' + empId);

                // update employee availability
                batch.update(admin.firestore().collection('employees').doc(empId), 'availability', admin.firestore.FieldValue.increment(skillEmp?.get('time')));
                batch.update(skillEmp?.ref, 'time', 0);
              }
            }
          }
        }
      }
    }

    await batch.commit();
    // console.log('Done!')
    return null;
  });
