import { AuthService } from '../../data/services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {
  private user;
  constructor(public auth: AuthService, public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this.auth.loadUser();
    this.auth.getLoggedUser().subscribe((user) => {
      this.user = user;
    });

    if (!this.user) {
      this.router.navigate(['auth/login']);
      return false;
    }  else if (!this.user.isAdmin) {
      this.router.navigate(['not-found']);
      return false;
    }

    return true;
  }
}
