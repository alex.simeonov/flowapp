import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(public auth: AngularFireAuth, public router: Router) {}

  canActivate(): Observable<boolean> {
    return this.auth.authState.pipe(
      take(1),
      map((user: User) => !!user),
      tap((authenticated) => {
        if (!authenticated) {
          this.router.navigate(['auth']);
        }
      })
    );
  }
}
