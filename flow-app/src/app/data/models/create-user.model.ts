import { Manager } from './manager.model';
import { Employee } from './employee.model';
import { Skill } from './skill.model';

export interface CreateUser {
  name: string;
  position: string;
  email: string;
  availability: number;
  password?: string;
  managerId?: string;
  isManager?: boolean;
  isAdmin?: boolean;
  manager?: Partial<Manager>;
  employees?: Partial<Employee>[];
  skills?: Partial<Skill>[];
  skillIds?: string[];
}
