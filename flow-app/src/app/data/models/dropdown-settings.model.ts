export interface DropdownSettings {
  enableSearchFilter: boolean;
  enableCheckAll: boolean;
  text: string;
  noDataLabel: string;
  labelKey: string;
  primaryKey: string;
  searchBy: string;
}
