import { Skill } from './skill.model';
import { Manager } from './manager.model';

export interface Employee {
  id: string;
  name: string;
  position: string;
  email: string;
  avatar: string;
  availability: number;
  manager: Partial<Manager>;
  skills: Partial<Skill>[];
  skillIds: string[];
  isManager: boolean;
  usedAvailability?: { [key: string]: number };
  projAvailability?: { [key: string]: { [key: string]: number } };
  ref?: Employee;
}
