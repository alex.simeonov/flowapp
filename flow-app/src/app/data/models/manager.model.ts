import { Employee } from './employee.model';

export interface Manager {
  id: string;
  name: string;
  position: string;
  email: string;
  password: string;
  isAdmin: boolean;
  avatar: string;
  availability: number;
  manager: Partial<Manager>;
  employees: Partial<Employee>[];
}
