import { Manager } from './manager.model';
import { Employee } from './employee.model';
import { Skill } from './skill.model';

export interface Project {
  id: string;
  name: string;
  description: string;
  target: number;
  planningStatus: boolean;
  progressStatus: boolean;
  startingDate: string;
  managementTime: number;
  daysToComplete?: number;
  elapsedDays?: number;
  employeeIds: string[];
  skillIds: string;
  manager: Partial<Manager>;
  employees: Partial<Employee>[];
  skills: Partial<Skill>[];
}
