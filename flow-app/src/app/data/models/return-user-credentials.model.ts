export interface ReturnUserCredentials {
  email: string;
  photoURL?: string;
  uid: string;
}
