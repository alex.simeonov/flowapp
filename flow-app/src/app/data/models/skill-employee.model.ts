import { Employee } from './employee.model';

export interface SkillEmployee {
  id: string;
  name: string;
  skillId: string;
  skillName: string;
  time: number;
  ref?: Employee;
}
