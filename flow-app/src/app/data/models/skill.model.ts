import { Employee } from './employee.model';

export interface Skill {
  id?: string;
  name: string;
  time?: number;
  progress?: number;
  employeeIds?: string[];
  employees: Partial<Employee>[];
}
