import { Manager } from './manager.model';
import { Employee } from './employee.model';
import { Skill } from './skill.model';

export interface Worker {
  id: string;
  name: string;
  position: string;
  email: string;
  password: string;
  isManager: boolean;
  isAdmin?: boolean;
  avatar?: string;
  availability: number;
  manager: Partial<Manager>;
  employees?: Partial<Employee>[];
  skills?: Partial<Skill>[];
  skillIds?: string[];
}
