import { Router } from '@angular/router';
import { BehaviorSubject, of} from 'rxjs';
import { Manager } from '../models/manager.model';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {
  public user = new BehaviorSubject<Manager>(null);
  private userId: string;

  constructor(
    private afs: AngularFirestore,
    private auth: AngularFireAuth,
    private router: Router
  ) {
    this.loadUser();
  }

  public loadUser() {
    this.auth.authState
      .pipe(
        switchMap((user) => {
          if (user) {
            this.userId = user.uid;
            return this.afs.collection('managers').doc(user.uid).valueChanges();
          } else {
            return of(null);
          }
        })
      )
      .subscribe((manager: Manager) => {
        if (manager) {
          this.user.next({ ...manager, id: this.userId });
        } else {
          this.user.next(null);
        }
      });
  }

  public getLoggedUser() {
    return this.user.asObservable();
  }

  public logout() {
    this.user.next(null);
    return this.auth.signOut();
  }

  public login(email: string, password: string): void {
    this.auth.signInWithEmailAndPassword(email, password).then(() => {
      this.router.navigate(['dashboard']);
    });
  }
}
