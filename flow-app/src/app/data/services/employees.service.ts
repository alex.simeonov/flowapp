import { Employee } from './../models/employee.model';
import { SkillsService } from './skills.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Skill } from '../models/skill.model';
import { take } from 'rxjs/operators';

import * as firebase from 'firebase/app';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  private userId: string;
  private employees = new BehaviorSubject<Employee[]>([]);
  private skillsFilter = new BehaviorSubject<string>('');
  private singleEmployee: BehaviorSubject<Employee>
    = new BehaviorSubject<Employee>(null);
  private loggedUserSubordinatesSub: Subscription;

  constructor(
    private afs: AngularFirestore,
    private auth: AngularFireAuth,
    private skillsService: SkillsService
  ) {
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.userId = user.uid;
      }
    });

    this.afs
      .collection('employees')
      .valueChanges({ idField: 'id' })
      .subscribe((employees: Employee[]) => this.employees.next(employees));
  }

  public getAllEmployees(filter: string) {
    if (filter) {
      const filteredEmployees = new BehaviorSubject<Employee[]>(null);

      this.employees.subscribe((employees: Employee[]) =>
        filteredEmployees.next(
          employees.filter((employee: Employee) =>
            employee.skillIds.includes(filter)
          )
        ));
      return filteredEmployees.asObservable();
    } else {
      return this.employees;
    }
  }

  public setSkillsFilterValue(skillId: string) {
    this.skillsFilter.next(skillId);
  }

  public getSkillsFilterValue() {
    return this.skillsFilter.asObservable().pipe(take(1));
  }

  public getLoggedUserSubordinates() {
    this.loggedUserSubordinatesSub?.unsubscribe();
    const subordinates = new BehaviorSubject<Employee[]>([]);

    this.loggedUserSubordinatesSub = this.employees.subscribe((employees: Employee[]) =>
      subordinates.next(
        employees.filter(
          (employee: Employee) => employee.manager.id === this.userId
        )
      )
    );

    return subordinates.asObservable();
  }

  public loadSingleEmployee(id: string) {
    this.afs
      .collection('employees')
      .doc(id)
      .valueChanges().pipe(take(1)).subscribe((employee: Employee) => {
        this.singleEmployee.next(employee);
      })
      ;
    // this.employees
    //   .pipe(take(1))
    //   .subscribe((employees: Employee[]) => {
    //     this.singleEmployee.next(
    //       employees.find((employee: Employee) => employee.id === id)
    //     );
    //   });
  }

  public getSingleEmployee(id: string) {
    this.loadSingleEmployee(id);
    return this.singleEmployee.asObservable();
  }

  public editEmployeeSkills(
    employeeId: string,
    employee: Partial<Employee>,
    skills: Skill[]
  ) {
    if (skills.length) {
      this.afs.collection('employees').doc(employeeId).update({
        skills: firebase.firestore.FieldValue.arrayUnion(
          ...skills.map((skill) => {
            return { name: skill.name, id: skill.id };
          })
        ),
        skillIds: firebase.firestore.FieldValue.arrayUnion(
          ...skills.map((skill) => skill.id)
        ),
      });

      skills.forEach((skill: Skill) => {
        this.skillsService.updateSkillEmployees(skill.id, {
          id: employeeId,
          name: employee.name,
        });
      });
    }
  }
}
