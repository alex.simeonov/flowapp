import { Employee } from '../../../app/data/models/employee.model';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { Manager } from './../models/manager.model';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';

import * as firebase from 'firebase/app';
import 'firebase/firestore';

@Injectable({ providedIn: 'root' })
export class ManagersService {
  private managers = new BehaviorSubject<Manager[]>([]);
  private manager = new BehaviorSubject<Manager>(null);

  constructor(private afs: AngularFirestore) {
    this.afs.collection('managers')
      .valueChanges({ idField: 'id' })
      .subscribe((managers: Manager[]) => this.managers.next(managers));
  }

  public getAllManagers() {
    return this.managers.asObservable();
  }

  public loadManagerById(id: string) {
    this.managers
      .pipe(take(1))
      .subscribe((managers: Manager[]) => {
        this.manager.next(managers.find((manager: Manager) => manager.id === id));
      });
  }

  public getManagerById(id: string) {
    this.loadManagerById(id);
    return this.manager.asObservable();
  }

  public updateManagerEmployees(id: string, employee: Partial<Employee>) {
    this.afs.collection('managers').doc(id).update({
      employees: firebase.firestore.FieldValue.arrayUnion(employee),
    });
  }
}
