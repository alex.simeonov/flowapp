import { ToastrService } from 'ngx-toastr';
import { Skill } from './../models/skill.model';
import { Project } from './../models/project.model';
import {
  BehaviorSubject,
  Subscription,
  combineLatest,
  Observable,
  from,
  of,
} from 'rxjs';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { take } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { SkillEmployee } from '../models/skill-employee.model';

@Injectable({ providedIn: 'root' })
export class ProjectsService {
  private projects = new BehaviorSubject<Project[]>([]);
  private singleProject = new BehaviorSubject<Project>(null);
  private fullProject = new BehaviorSubject<[any, any[], any[]]>([
    null,
    null,
    null,
  ]);
  private activeUserProjectsSub: Subscription;
  private loggedUserProjects = new BehaviorSubject<Project[]>([]);
  private project: Observable<any>;
  private skillEmployee: Observable<any[]>;
  private skills: Observable<any[]>;
  private projectId: string;

  constructor(private afs: AngularFirestore, private toastr: ToastrService) {
    this.afs
      .collection('projects')
      .valueChanges({ idField: 'id' })
      .subscribe((projects: Project[]) => this.projects.next(projects));
  }

  public getAllProjects(skillid?: string, status?: string) {
    return this.projects.asObservable();
  }

  public loadSingleProject(projectId: string) {
    this.projectId = projectId;

    this.projects.pipe(take(1)).subscribe((projects: Project[]) => {
      this.singleProject.next(
        projects.find((project: Project) => project.id === projectId)
      );
    });
  }

  public getSingleProject(projectId: string) {
    this.loadSingleProject(projectId);
    return this.singleProject.asObservable();
  }

  public loadLoggedUserProjects(userId: string) {
    this.loggedUserProjects.next(
      this.projects.value.filter(
        (project: Project) => project.manager.id === userId
      )
    );
  }
  public getLoggedUserProjects(userId: string) {
    this.loadLoggedUserProjects(userId);
    return this.loggedUserProjects.asObservable();
  }

  public getEmployeeActiveProjects(employeeId: string) {
    this.activeUserProjectsSub?.unsubscribe();
    const projs = new BehaviorSubject<Project[]>([]);

    this.activeUserProjectsSub = this.projects.subscribe(
      (projects: Project[]) => {

        projs.next(
          projects.filter(
            (project: Project) =>
              project.employeeIds?.includes(employeeId) &&
              !project.progressStatus
          )
        );
      }
    );
    return projs.asObservable();
  }

  public getManagerActiveProjects(managerId: string) {
    this.activeUserProjectsSub?.unsubscribe();
    const projs = new BehaviorSubject<Project[]>([]);

    this.activeUserProjectsSub = this.projects.subscribe(
      (projects: Project[]) => {
        projs.next(
          projects.filter(
            (project: Project) =>
              project.manager.id === managerId && !project.progressStatus
          )
        );
      }
    );
    return projs.asObservable();
  }

  public createProject(project: any, employeeHours: any, skillHours: any) {
    const employeeIds = new Set<string>();
    const skillIds = new Set<string>();

    const batch: firebase.firestore.WriteBatch = this.afs.firestore.batch();
    const projectRef: DocumentReference = this.afs.firestore
      .collection('projects')
      .doc();

    // subtract availability from manager
    const managerRef = this.afs.firestore
      .collection('managers')
      .doc(project.manager.id);
    batch.update(
      managerRef,
      'availability',
      firebase.firestore.FieldValue.increment(-project.managementTime)
    );

    for (const employeeHour of employeeHours) {
      employeeIds.add(employeeHour.id);

      const skill = {
        id: employeeHour.id,
        name: employeeHour.name,
        skillId: employeeHour.skillId,
        skillName: employeeHour.skillName,
        time: employeeHour.time,
      };
      const employeeHourRef = projectRef
        .collection('skill_employee')
        .doc(skill.skillId + '_' + skill.id);
      batch.set(employeeHourRef, skill);

      // subtract availability from employee
      const employeeRef: DocumentReference = this.afs.firestore
        .collection('employees')
        .doc(skill.id);
      batch.update(
        employeeRef,
        'availability',
        firebase.firestore.FieldValue.increment(-skill.time)
      );
    }

    for (const skillHour of skillHours) {
      skillIds.add(skillHour.id);
      skillHour.progress = 0;

      const skillHourRef = projectRef.collection('skills').doc(skillHour.id);
      batch.set(skillHourRef, skillHour);
    }

    project.startingDate = firebase.firestore.FieldValue.serverTimestamp();
    project.employeeIds = Array.from(employeeIds);
    project.skillIds = Array.from(skillIds);
    project.elapsedDays = 0;

    batch.set(projectRef, project);

    batch.commit().then(() => this.toastr.success('Project created successfully.')).catch(() => this.toastr.error('Project could not be created.'));
  }

  public editProject(project: any, employeeHours: any, skillHours: any) {
    const employeeIds = new Set<string>();
    const skillIds = new Set<string>();

    const batch: firebase.firestore.WriteBatch = this.afs.firestore.batch();
    const projectRef: DocumentReference = this.afs.firestore
      .collection('projects')
      .doc(this.projectId);

    // subtract availability from manager
    const managerRef = this.afs.firestore
      .collection('managers')
      .doc(project.manager.id);
    batch.update(
      managerRef,
      'availability',
      firebase.firestore.FieldValue.increment(-project.managementTime)
    );

    for (const employeeHour of employeeHours) {
      employeeIds.add(employeeHour.id);

      const skill = {
        id: employeeHour.id,
        name: employeeHour.name,
        skillId: employeeHour.skillId,
        skillName: employeeHour.skillName,
        time: employeeHour.time,
      };
      const employeeHourRef = projectRef
        .collection('skill_employee')
        .doc(skill.skillId + '_' + skill.id);
      batch.set(employeeHourRef, skill);

      // subtract availability from employee
      const employeeRef: DocumentReference = this.afs.firestore
        .collection('employees')
        .doc(skill.id);
      batch.update(
        employeeRef,
        'availability',
        firebase.firestore.FieldValue.increment(-skill.time)
      );
    }

    for (const skillHour of skillHours) {
      skillIds.add(skillHour.id);

      if (!skillHour.progress) {
        skillHour.progress = 0;
      }

      const skillHourRef = projectRef.collection('skills').doc(skillHour.id);
      batch.set(skillHourRef, skillHour);
    }

    project.employeeIds = Array.from(employeeIds);
    project.skillIds = Array.from(skillIds);
    // project.elapsedDays = project.elapsedDays;
    // console.log(project);

    batch.set(projectRef, project);

    this.stopProject(true).then(() =>
      batch.commit().then(() => this.toastr.success('Project updated successfully.'))).catch(() => this.toastr.error('Project could not be updated.'));
  }

  public loadProject(projectId: string) {
    this.projectId = projectId;

    this.project = this.afs
      .collection('projects')
      .doc(projectId)
      .valueChanges();

    this.skillEmployee = this.afs
      .collection('projects')
      .doc(projectId)
      .collection('skill_employee')
      .valueChanges();

    this.skills = this.afs
      .collection('projects')
      .doc(projectId)
      .collection('skills')
      .valueChanges({ idField: 'id' });

    combineLatest([this.project, this.skillEmployee, this.skills]).subscribe(
      (data) => {
        this.fullProject.next(data);
      }
    );
  }

  public getProject(projectId: string) {
    this.loadProject(projectId);
    return this.fullProject.asObservable();
  }

  public stopProject(reset?: boolean): Promise<void> {
    const project = this.fullProject.value[0];
    const skillsEmployees = this.fullProject.value[1];
    const skills = this.fullProject.value[2];

    const batch: firebase.firestore.WriteBatch = this.afs.firestore.batch();

    const projectRef: DocumentReference = this.afs.firestore.collection('projects').doc(this.projectId);
    batch.update(projectRef, 'managementTime', 0);

    if (!reset) {
      batch.update(projectRef, 'progressStatus', true);
    }

    const managerRef = this.afs.firestore.collection('managers').doc(project.manager.id);
    batch.update(managerRef, 'availability', firebase.firestore.FieldValue.increment(+project.managementTime));

    skillsEmployees.forEach((skEmp: SkillEmployee) => {
      batch.update(this.afs.firestore.collection('employees')
        .doc(skEmp.id), 'availability', firebase.firestore.FieldValue.increment(skEmp.time));

      if (reset) {
        batch.delete(this.afs.firestore.collection('projects').doc(this.projectId).collection('skill_employee')
          .doc(skEmp.skillId + '_' + skEmp.id));
      } else {
        batch.update(this.afs.firestore.collection('projects').doc(this.projectId).collection('skill_employee')
          .doc(skEmp.skillId + '_' + skEmp.id), 'time', firebase.firestore.FieldValue.increment(-skEmp.time));
      }
    });

    if (reset) {
      skills.forEach((skill: Skill) => {
        batch.delete(this.afs.firestore.collection('projects').doc(this.projectId).collection('skills')
          .doc(skill.id));
      });
    }

    return batch.commit();
  }
}
