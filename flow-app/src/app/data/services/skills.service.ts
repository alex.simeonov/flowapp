import { ToastrService } from 'ngx-toastr';
import { Employee } from 'src/app/data/models/employee.model';
import { Skill } from './../models/skill.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import * as firebase from 'firebase/app';
import 'firebase/firestore';

@Injectable({ providedIn: 'root' })
export class SkillsService {
  private skills = new BehaviorSubject<Skill[]>([]);

  constructor(private afs: AngularFirestore, private toastr: ToastrService) {
    this.afs.collection('skills')
      .valueChanges({ idField: 'id' })
      .subscribe((skills: any[]) => this.skills.next(skills));
  }

  getAllSkills(): Observable<Skill[]> {
    return this.skills.asObservable();
  }

  public createSkill(name: string) {
    const newSkill: Skill = {
      name,
      employees: []
    };
    this.afs.collection('skills').add(newSkill)
      .then(() => {
        this.toastr.success('Skill created successfuly.');
      })
      .catch(() => {
        this.toastr.error('Skill could not be created.');
      });
  }

  public updateSkillEmployees(id: string, employee: Partial<Employee>) {
    this.afs.collection('skills').doc(id).update({ employees: firebase.firestore.FieldValue.arrayUnion(employee) });
  }
}
