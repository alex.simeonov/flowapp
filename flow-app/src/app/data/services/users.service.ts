import { ToastrService } from 'ngx-toastr';
import { SkillsService } from './skills.service';
import { ManagersService } from './managers.service';
import { CreateUser } from '../models/create-user.model';
import { Manager } from '../models/manager.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private user$: Observable<Manager>;

  constructor(
    private afs: AngularFirestore,
    private managersService: ManagersService,
    private skillsService: SkillsService,
    private toastr: ToastrService
  ) {}

  public createManager(user: CreateUser, userUID: string) {
    this.afs
      .collection('managers')
      .doc(userUID)
      .set(user)
      .then(() => {
        this.toastr.success('Manager created successfuly.');
      })
      .catch(() => {
        this.toastr.error('Manager could not be created.');
      });
  }

  public createEmployee(user: CreateUser) {
    this.afs
      .collection('employees')
      .add(user)
      .then((userRef) => {
        this.managersService.updateManagerEmployees(user.manager.id, {
          id: userRef.id,
          name: user.name,
        });
        user.skillIds.forEach((id: string) => {
          this.skillsService.updateSkillEmployees(id, {
            id: userRef.id,
            name: user.name,
          });
        });
        this.toastr.success('Employee created successfuly.');
      })
      .catch(() => {
        this.toastr.error('Manager could not be created.');
      });
  }
}
