import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { SharedModule } from '../../shared/shared.module';

// const routes: Routes = [
//   {
//     path: 'login',
//     component: LoginComponent
//   }
// ]

@NgModule({
  declarations: [LoginComponent],
  imports: [
    SharedModule,
    // RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthModule { }
