import { Router } from '@angular/router';
import { Project } from '../../../../data/models/project.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-active-project',
  templateUrl: './active-project.component.html',
  styleUrls: ['./active-project.component.scss']
})
export class ActiveProjectComponent implements OnInit {
  @Input() public singleProject: Project;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public onProject() {
    this.router.navigate(['projects/', this.singleProject.id]);
  }
}
