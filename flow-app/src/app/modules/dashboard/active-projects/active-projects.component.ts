import { Manager } from './../../../data/models/manager.model';
import { Project } from './../../../data/models/project.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-active-projects',
  templateUrl: './active-projects.component.html',
  styleUrls: ['./active-projects.component.scss'],
})
export class ActiveProjectsComponent implements OnInit {
  @Input() public activeProjects: Project[];
  public userId: string;
  @Input() public manager: Manager;

  constructor() {}

  ngOnInit(): void { }

}
