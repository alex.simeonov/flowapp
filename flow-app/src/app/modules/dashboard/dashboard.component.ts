import { AngularFireAuth } from '@angular/fire/auth';
import { ProjectsService } from './../../data/services/projects.service';
import { AuthService } from '../../data/services/auth.service';
import { Manager } from './../../data/models/manager.model';
import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Project } from 'src/app/data/models/project.model';
import { mergeMap, tap, switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  public currentManager: Manager;
  public projects: Project[];
  private sub = new Subscription();

  constructor(private authService: AuthService, private projectsService: ProjectsService, private auth: AngularFireAuth) { }

  ngOnInit(): void {
    this.sub.add(this.authService.getLoggedUser()
      .pipe(
        tap(manager => this.currentManager = manager),
        switchMap((user: any) => this.projectsService.getManagerActiveProjects(user?.id)))
      .subscribe((projects: Project[]) => this.projects = projects));
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
