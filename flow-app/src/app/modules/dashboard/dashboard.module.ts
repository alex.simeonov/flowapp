import { AuthGuard } from './../../core/guards/auth-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ManagerComponent } from './manager/manager.component';
import { DashboardComponent } from './dashboard.component';
import { ActiveProjectsComponent } from './active-projects/active-projects.component';
import { ActiveProjectComponent } from './active-projects/active-project/active-project.component';
import { SharedModule } from '../../shared/shared.module';
import { SubordinatesComponent } from './manager/subordinates/subordinates.component';
import { SingleSubordinateComponent } from './manager/subordinates/single-subordinate/single-subordinate.component';

// const routes: Routes = [
//   {
//     path: '',
//     component: DashboardComponent,
//   }
// ];

@NgModule({
  declarations: [
    DashboardComponent,
    ActiveProjectsComponent,
    ActiveProjectComponent,
    ManagerComponent,
    SubordinatesComponent,
    SingleSubordinateComponent
  ],
  imports: [
    SharedModule,
    // RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
