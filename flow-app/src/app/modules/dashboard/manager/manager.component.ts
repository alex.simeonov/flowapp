import { Component, OnInit, Input } from '@angular/core';
import { Manager } from '../../../../app/data/models/manager.model';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent implements OnInit {
  @Input() public manager: Manager;
  public managerId: string;

  constructor() { }

  ngOnInit(): void { }

  public onSubordinate(event: Event) {
    //
  }
}
