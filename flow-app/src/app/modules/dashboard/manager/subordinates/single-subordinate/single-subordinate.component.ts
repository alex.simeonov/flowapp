import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { Employee } from 'src/app/data/models/employee.model';

@Component({
  selector: 'app-single-subordinate',
  templateUrl: './single-subordinate.component.html',
  styleUrls: ['./single-subordinate.component.scss']
})
export class SingleSubordinateComponent implements OnInit {
  @Input() public singleSubordinate: Employee;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public onSubordinateClick() {
    this.router.navigate(['employees/', this.singleSubordinate.id]);
  }
}
