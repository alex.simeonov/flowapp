import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../../../../../app/data/models/employee.model';

@Component({
  selector: 'app-subordinates',
  templateUrl: './subordinates.component.html',
  styleUrls: ['./subordinates.component.scss']
})
export class SubordinatesComponent implements OnInit {
  @Input() public subordinates: Employee[];
  constructor() { }

  ngOnInit(): void {
  }

}
