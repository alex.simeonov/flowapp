import { Skill } from './../../../data/models/skill.model';
import { ManagersService } from './../../../data/services/managers.service';
import { Manager } from './../../../data/models/manager.model';
import { CreateUser } from './../../../data/models/create-user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-employee-dialog',
  templateUrl: './create-employee-dialog.component.html',
  styleUrls: ['./create-employee-dialog.component.scss'],
})
export class CreateEmployeeDialogComponent implements OnInit {
  public isManager = false;
  public isAdmin = false;
  public user: CreateUser = null;
  public managers: Manager[];
  public manager: Manager;
  public directManager: Partial<Manager>;
  public directManagerName: string;
  public directManagerId: string;
  public skills: any[];

  createEmployee: FormGroup = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
    position: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(9),
      Validators.maxLength(20),
      Validators.pattern(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      ),
    ]),
    password: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(20),
    ]),
  });

  constructor(
    public modal: NgbActiveModal,
    private managersService: ManagersService
  ) {}

  ngOnInit(): void {
    this.managersService.getAllManagers().subscribe((managers: Manager[]) => {
      this.managers = managers;
    });
  }

  public signUp() {
    if (this.isManager) {
      this.user = {
        name: this.createEmployee.value.name,
        position: this.createEmployee.value.position,
        email: this.createEmployee.value.email,
        password: this.createEmployee.value.password,
        isManager: this.isManager,
        isAdmin: this.isAdmin,
        availability: 8,
        manager: this.directManager,
        employees: [],
      };
    } else {
      this.user = {
        name: this.createEmployee.value.name,
        position: this.createEmployee.value.position,
        email: this.createEmployee.value.email,
        availability: 8,
        isManager: false,
        manager: this.directManager,
        skills: this.skills
          ? this.skills.map((skill: Skill) => {
              return { name: skill.name, id: skill.id };
            })
          : [],
        skillIds: this.skills
          ? this.skills.map((skill: Skill) => skill.id)
          : [],
      };
    }

    this.modal.close(this.user);
  }

  public onManager() {
    this.isManager = !this.isManager;
  }

  public onAdmin() {
    this.isAdmin = !this.isAdmin;
  }

  public onManagerSelect(event: Event) {
    this.directManagerName = (event.target as HTMLInputElement).value;
    if (this.directManagerName !== 'Self Managed') {
      this.directManagerId = this.managers.find(
        (manager: Manager) => manager.name === this.directManagerName
      ).id;
    } else {
      this.directManagerId = '';
    }

    this.directManager = {
      name: this.directManagerName,
      id: this.directManagerId,
    };
  }

  public onSkillsSelected(skills: Skill[]) {
    if (!skills.length) {
      this.skills = [];
    } else {
      this.skills = skills;
    }
  }
}
