import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { Skill } from '../../../../data/models/skill.model';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import {
  FormGroup,
  FormBuilder,
  FormGroupDirective,
  FormArray,
} from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { SkillsService } from '../../../../data/services/skills.service';

@Component({
  selector: 'app-skills-select',
  templateUrl: './skills-select.component.html',
  styleUrls: ['./skills-select.component.scss'],
})
export class SkillsSelectComponent implements OnInit {
  public employeeSkills: FormGroup;
  public allSkills: Skill[];
  @Output() public chosenSkills: EventEmitter<Skill[]> = new EventEmitter<
    Skill[]
  >();

  @ViewChild('searchInput') public searchInput: ElementRef;
  private search$ = new Subject<string>();

  public dropdownList = [];
  public dropdownSettings = {};

  constructor(
    private fb: FormBuilder,
    private fgd: FormGroupDirective,
    private cd: ChangeDetectorRef,
    private skillsService: SkillsService
  ) {}

  ngOnInit(): void {
    this.employeeSkills = this.fgd.form;
    this.employeeSkills.addControl('skills', this.fb.control(''));

    this.skillsService.getAllSkills().subscribe((skills: Skill[]) => {
      this.allSkills = [...skills];  // da mahna tiq deto gi ima
      this.dropdownList = [...this.allSkills];
    });

    this.dropdownSettings = {
      enableSearchFilter: true,
      enableCheckAll: false,
      text: 'Select skills',
      noDataLabel: 'No such skill',
      labelKey: 'name',
      primaryKey: 'id',
      // searchBy: ['name'],
    };

    this.search$
      .pipe(debounceTime(100), distinctUntilChanged())
      .subscribe((filterValue: string) => {
        this.dropdownList = this.allSkills.filter((item: any) =>
          item.name.toUpperCase().includes(filterValue)
        );
        this.cd.detectChanges();
      });
  }

  public onSearch(event: KeyboardEvent) {
    const filterValue: string = (event.target as HTMLInputElement).value
      .trim()
      .toUpperCase();
    this.search$.next(filterValue);
  }

  public onOpen() {
    setTimeout(() => {
      this.searchInput.nativeElement.focus();
    }, 0);
  }

  public onSelect() {
    this.chosenSkills.emit(this.employeeSkills.value.skills);
  }

  public onDeSelect() {
    this.chosenSkills.emit(this.employeeSkills.value.skills);
  }

  public onClose() {
    this.chosenSkills.emit(this.employeeSkills.value.skills);
  }
}
