import { Employee } from 'src/app/data/models/employee.model';
import { Skill } from './../../../../data/models/skill.model';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Input,
} from '@angular/core';
import { FormGroup, FormBuilder, FormGroupDirective } from '@angular/forms';
import { Subject, BehaviorSubject } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SkillsService } from 'src/app/data/services/skills.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-edit-employee-skills',
  templateUrl: './edit-employee-skills.component.html',
  styleUrls: ['./edit-employee-skills.component.scss'],
})
export class EditEmployeeSkillsComponent implements OnInit {
  public employeeSkills: FormGroup;
  @Input() public allSkills: Skill[];
  @Input() public currentEmployee: Employee;
  @Input() public recentlyAddedSkills: string[];
  @Output() public chosenSkills: EventEmitter<Skill[]> = new EventEmitter<
    Skill[]
  >();

  @ViewChild('searchInput') public searchInput: ElementRef;
  private search$ = new Subject<string>();

  public dropdownList = [];
  public dropdownSettings = {};

  constructor(
    private fb: FormBuilder,
    private fgd: FormGroupDirective,
    private cd: ChangeDetectorRef,
    private skillsService: SkillsService,
  ) {}

  ngOnInit(): void {
    this.employeeSkills = this.fgd.form;
    this.employeeSkills.addControl('skills', this.fb.control(''));

    this.skillsService.getAllSkills().subscribe((skills: Skill[]) => {
      this.allSkills = [...skills.filter((skill: Skill) => !this.currentEmployee?.skillIds.includes(skill.id))];
      this.dropdownList = [...this.allSkills.filter((skill: Skill) => !this.recentlyAddedSkills.includes(skill.name))];
    });

    this.dropdownSettings = {
      enableSearchFilter: true,
      enableCheckAll: false,
      text: 'Select skills',
      noDataLabel: 'No such skill',
      labelKey: 'name',
      primaryKey: 'id',
      // searchBy: ['name'],
    };

    this.search$
      .pipe(debounceTime(100), distinctUntilChanged())
      .subscribe((filterValue: string) => {
        this.dropdownList = this.allSkills.filter((item: any) =>
          item.name.toUpperCase().includes(filterValue)
        );
        this.cd.detectChanges();
      });
  }

  public onSearch(event: KeyboardEvent) {
    const filterValue: string = (event.target as HTMLInputElement).value
      .trim()
      .toUpperCase();
    this.search$.next(filterValue);
  }

  public onOpen() {
    setTimeout(() => {
      this.searchInput.nativeElement.focus();
    }, 0);
  }

  public onSelect() {
    this.chosenSkills.emit(this.employeeSkills.value.skills);
  }

  public onDeSelect() {
    this.chosenSkills.emit(this.employeeSkills.value.skills);
  }

  public onClose() {
    this.chosenSkills.emit(this.employeeSkills.value.skills);
  }
}
