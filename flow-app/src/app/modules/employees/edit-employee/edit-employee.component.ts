import { EmployeesService } from './../../../data/services/employees.service';
import { ActivatedRoute } from '@angular/router';
import { Employee } from 'src/app/data/models/employee.model';
import { Skill } from './../../../data/models/skill.model';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss'],
})
export class EditEmployeeComponent implements OnInit, OnDestroy {
  @Input() public skills: Skill[];
  public editEmployee: FormGroup;
  @Input() public employeeId: string;
  @Input() public employee: BehaviorSubject<Employee>;
  public employeeSkills: BehaviorSubject<Partial<Skill>[]> = new BehaviorSubject<Partial<Skill>[]>([]);
  public displayedSkills: string;
  private sub: Subscription = new Subscription();

  constructor(
    public modal: NgbActiveModal,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private employeesService: EmployeesService
  ) {}

  ngOnInit() {
    this.displayedSkills = this.employee.value?.skills
      ?.map((skill) => skill.name)
      .join(', ');
    this.editEmployee = this.fb.group({});
    this.employeeSkills.next(this.employee.value?.skills);
    // this.employeeId = this.route.snapshot.params['id'];
    // this.sub.add(
    // this.employeesService
    //   .getSingleEmployee(this.employeeId)
    //   .subscribe((employee: Employee) => {
    //     this.employee = employee;
    //     this.displayedSkills = this.employee?.skills
    //       ?.map((skill) => skill.name)
    //       .join(', ');
    //       console.log(this.employee);
    //     })
    // );
    // this.route.data.subscribe((data: {employee: Employee, skills: Skill[]}) => {
    //   this.employee = data.employee;
    //   this.skills = data.skills;
    // });
  }

  public onSkillsSelected(skills: Skill[]) {
    if (!skills.length) {
      this.skills = [];
    } else {
      this.skills = skills;
    }
  }

  onSubmit() {
    this.employeesService.editEmployeeSkills(
      this.employeeId,
      this.employee.value,
      this.editEmployee.value.skills
    );
    const newSkills = this.editEmployee.value.skills.filter((skill: Partial<Skill>) => !this.employee.value.skills.includes(skill));
    this.modal.close({...this.employee, skills: [...this.employee.value.skills, ...newSkills]});
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
