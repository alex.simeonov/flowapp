import { EditEmployeeComponent } from '../../../../app/modules/employees/edit-employee/edit-employee.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Manager } from './../../../data/models/manager.model';
import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../../../../app/data/models/employee.model';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss']
})
export class EmployeeCardComponent implements OnInit {
  @Input() public singleEmployee: any;
  public employeeSkills: string;
  @Input() public manager: Manager;

  constructor(private auth: AngularFireAuth, private modalService: NgbModal) { }

  ngOnInit(): void { }

}
