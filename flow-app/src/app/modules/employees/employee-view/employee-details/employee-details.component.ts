import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Manager } from './../../../../data/models/manager.model';
import { EmployeesService } from './../../../../data/services/employees.service';
import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EditEmployeeComponent } from '../../edit-employee/edit-employee.component';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss'],
})
export class EmployeeDetailsComponent implements OnInit {
  @Input() public currentEmployee: any;
  @Input() public loggedUser: Manager;
  @Output() public onEdit = new EventEmitter<void>();

  constructor(private router: Router, private employeesService: EmployeesService) { }

  ngOnInit(): void { }

  public onSkillSelect(skillId: string) {
    this.router.navigate(['/employees']);
    this.employeesService.setSkillsFilterValue(skillId);
  }

  public onEmployeeSelect(employeeId: string) {
    this.router.navigateByUrl('/', { skipLocationChange: true })
      .then(() => this.router.navigate(['/employees', employeeId]));
  }

  onEditEmployee() {
    this.onEdit.emit();
  }
}
