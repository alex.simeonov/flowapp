import { AuthService } from '../../../data/services/auth.service';
import { Manager } from './../../../data/models/manager.model';
import { BehaviorSubject } from 'rxjs';
import { ManagersService } from './../../../data/services/managers.service';
import { Skill } from './../../../data/models/skill.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectsService } from './../../../data/services/projects.service';
import { Project } from '../../../../app/data/models/project.model';
import { EmployeesService } from './../../../data/services/employees.service';
import { Employee } from '../../../../app/data/models/employee.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { EditEmployeeComponent } from '../edit-employee/edit-employee.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.scss'],
})
export class EmployeeViewComponent implements OnInit, OnDestroy {
  public employee = new BehaviorSubject<any>(null);
  public employeeId: string;
  public activeProjects: Project[] = [];
  public skills: Skill[];
  public manager: Manager;

  constructor(
    private employeesService: EmployeesService,
    private projectsService: ProjectsService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private managersService: ManagersService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.employeeId = this.route.snapshot.params.id;

    this.employeesService
      .getSingleEmployee(this.employeeId)
      .subscribe((employee: Employee) => {
        if (employee) {
          this.employee.next(employee);
        } else {
          this.managersService
            .getManagerById(this.employeeId)
            .subscribe((manager) => (this.employee.next(manager)));
        }
      });

    if (!this.employee.value?.isManager) {
      this.projectsService
        .getEmployeeActiveProjects(this.employeeId)
        .subscribe((projects: Project[]) => {
          this.activeProjects = projects;
        });
    } else {
      this.projectsService
        .getManagerActiveProjects(this.employeeId)
        .subscribe((projects: Project[]) => {
          this.activeProjects = projects;
        });
    }

    this.authService.getLoggedUser().subscribe((user) => {
      this.manager = user;
    });
  }

  public onEditEmployee() {
    const ref = this.modalService.open(EditEmployeeComponent, {
      centered: true,
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
    });
    ref.componentInstance.employee = this.employee;
    ref.componentInstance.skills = this.skills;
    ref.componentInstance.employeeId = this.employeeId;
    ref.result
      .then((result) => {
        this.employee.value.skills = result.skills;
      })
      .catch((e: Error) => {
        if (e.message !== 'dismissed') {
          throw e;
        }
      });
  }

  ngOnDestroy() {
    this.employee.next(null);
  }
}
