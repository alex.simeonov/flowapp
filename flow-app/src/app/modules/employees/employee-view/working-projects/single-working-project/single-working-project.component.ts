import { Router } from '@angular/router';
import { Project } from 'src/app/data/models/project.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-single-working-project',
  templateUrl: './single-working-project.component.html',
  styleUrls: ['./single-working-project.component.scss']
})
export class SingleWorkingProjectComponent implements OnInit {
  @Input() public singleProject: Project;
  @Input() public employee: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public onProject(projectId: string) {
    this.router.navigate(['projects', projectId]);
  }
}
