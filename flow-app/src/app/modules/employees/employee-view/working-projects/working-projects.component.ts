import { Project } from 'src/app/data/models/project.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-working-projects',
  templateUrl: './working-projects.component.html',
  styleUrls: ['./working-projects.component.scss'],
})
export class WorkingProjectsComponent implements OnInit {
  @Input() public projects: Project[];
  @Input() public currentEmployee: any;

  constructor() {}

  ngOnInit(): void {}
}
