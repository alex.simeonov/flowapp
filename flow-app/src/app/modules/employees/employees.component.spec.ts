import { createUser } from './../../../../functions/src/index';
import { EmployeeHoursComponent } from './../projects/create-project/employee-hours/employee-hours.component';
import { environment } from './../../../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { NgbModal, NgbModule, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ManagersService } from './../../data/services/managers.service';
import { SkillsService } from '../../../app/data/services/skills.service';
import { EmployeesService } from './../../data/services/employees.service';
import { SharedModule } from './../../shared/shared.module';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { EmployeesComponent } from './employees.component';
import { of } from 'rxjs';
import { CreateEmployeeDialogComponent } from './create-employee-dialog/create-employee-dialog.component';
import { EmployeeCardComponent } from './employee-card/employee-card.component';
import { EmployeeViewComponent } from './employee-view/employee-view.component';
import { WorkingProjectsComponent } from './employee-view/working-projects/working-projects.component';
import { SkillsSelectComponent } from './create-employee-dialog/skills-select/skills-select.component';
import { EmployeeDetailsComponent } from './employee-view/employee-details/employee-details.component';
import { SingleWorkingProjectComponent } from './employee-view/working-projects/single-working-project/single-working-project.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { EditEmployeeSkillsComponent } from './edit-employee/edit-employee-skills/edit-employee-skills.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { RouterTestingModule } from '@angular/router/testing';
import { UsersService } from '../../data/services/users.service';
import { AuthService } from '../../data/services/auth.service';
import {
  AngularFireFunctions,
  AngularFireFunctionsModule,
} from '@angular/fire/functions';
import { AngularFireModule } from '@angular/fire';

describe('EmployeesComponent', () => {
  let usersService;
  let employeesService;
  let authService;
  let skillsService;
  let managersService;
  let modalService;
  let aFireFunctions;

  const routes: Routes = [];

  let fixture: ComponentFixture<EmployeesComponent>;
  let component: EmployeesComponent;

  beforeEach(async(() => {
    jest.clearAllMocks();

    authService = {
      loadUser() {
        /* empty */
      },
      getLoggedUser() {
        /* empty */
        return of(null);
      },
    };

    employeesService = {
      getAllEmployees() {
        /* empty */
      },
      setSkillsFilterValue() {
        /* empty */
      },
      getSkillsFilterValue() {
        /* empty */
      },
      getLoggedUserSubordinates() {
        /* empty */
      },
      loadSingleEmployee() {
        /* empty */
      },
      getSingleEmployee() {
        /* empty */
      },
      editEmployeeSkills() {
        /* empty */
      },
    };

    usersService = {
      createManager() {
        /* empty */
      },
      createEmployee() {
        /* empty */
      },
    };

    skillsService = {
      getAllSkills() {
        /* empty */
        return of(null);
      },
      updateEmployeeSkills() {
        /* empty */
      },
    };

    managersService = {
      getAllManagers() {
        /* empty */
      },
      loadManagerById() {
        /* empty */
      },
      getManagerById() {
        /* empty */
      },
      updateManagerEmployees() {
        /* empty */
      },
    };

    modalService = {
      open() {
        return NgbModalRef;
      },
      httpsCallable() {
        /* empty */
      },
    };

    aFireFunctions = {
      httpsCallable(arg: string) {
        /* empty */
      },
    };

    TestBed.configureTestingModule({
      declarations: [
        EmployeesComponent,
        CreateEmployeeDialogComponent,
        EmployeeCardComponent,
        EmployeeViewComponent,
        SkillsSelectComponent,
        WorkingProjectsComponent,
        EmployeeDetailsComponent,
        SingleWorkingProjectComponent,
        EditEmployeeComponent,
        EditEmployeeSkillsComponent,
      ],
      imports: [
        SharedModule,
        FormsModule,
        AngularFireModule.initializeApp(environment.firebaseConfig, 'FlowApp'),
        AngularFirestoreModule,
        AngularFireFunctionsModule,
        RouterTestingModule.withRoutes(routes),
        ReactiveFormsModule,
        NgBootstrapFormValidationModule.forRoot(),
        NgbModule,
      ],
      providers: [
        UsersService,
        EmployeesService,
        AuthService,
        SkillsService,
        ManagersService,
        NgbModal,
        AngularFireFunctions,
      ],
    })
      .overrideProvider(UsersService, { useValue: usersService })
      .overrideProvider(EmployeesService, { useValue: employeesService })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(SkillsService, { useValue: skillsService })
      .overrideProvider(ManagersService, { useValue: managersService })
      .overrideProvider(NgbModal, { useValue: modalService })
      .overrideProvider(AngularFireFunctions, { useValue: aFireFunctions })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EmployeesComponent);
        component = fixture.componentInstance;
        (component as any).employeesSource = [];
        (component as any).combineLatest = () => of(null);
      });
  }));

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('ngOnInit()', () => {
    it('should call employeesService.getAllEmployees()', () => {
      const spy = jest.spyOn(employeesService, 'getAllEmployees');

      component.ngOnInit();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('');
    });
    it('should call managersService.getAllManagers()', () => {
      const spy = jest.spyOn(managersService, 'getAllManagers');

      component.ngOnInit();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith();
    });
    it('should call employeesService.getSkillsFilterValue()', () => {
      const spy = jest.spyOn(employeesService, 'getSkillsFilterValue');

      component.ngOnInit();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith();
    });
    it('should call authService.loadUser()', () => {
      const spy = jest.spyOn(authService, 'loadUser');

      component.ngOnInit();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith();
    });
    it('should call authService.getLoggedUser()', () => {
      const spy = jest.spyOn(authService, 'getLoggedUser');

      component.ngOnInit();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith();
    });
    it('should call skillsService.getAllSkills()', () => {
      const spy = jest.spyOn(skillsService, 'getAllSkills');

      component.ngOnInit();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith();
    });
    it('should call onSkillFilter()', () => {
      const spy = jest.spyOn(component, 'onSkillFilter');

      component.ngOnInit();

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith();
    });
  });
});
