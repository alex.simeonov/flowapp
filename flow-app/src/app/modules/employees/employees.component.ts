import { ToastrService } from 'ngx-toastr';
import { debounceTime } from 'rxjs/operators';
import { ManagersService } from './../../data/services/managers.service';
import { SkillsService } from './../../data/services/skills.service';
import { AuthService } from '../../data/services/auth.service';
import { ReturnUserCredentials } from './../../data/models/return-user-credentials.model';
import { CreateUser } from './../../data/models/create-user.model';
import { Manager } from './../../data/models/manager.model';
import { EmployeesService } from './../../data/services/employees.service';
import { Employee } from './../../data/models/employee.model';
import { UsersService } from '../../data/services/users.service';
import { CreateEmployeeDialogComponent } from './create-employee-dialog/create-employee-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Skill } from '../../../app/data/models/skill.model';
import {
  combineLatest,
  Subject,
  Observable,
  BehaviorSubject,
} from 'rxjs';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
})
export class EmployeesComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('skillSelect') public skillSelect: HTMLSelectElement;
  public userData = {
    email: 'test2@test.test', // must check if there's already a user with this email
    password: 'password', // 6 chars min
    displayName: 'Tester',
    // photoURL: '',
  };
  public employees = new BehaviorSubject<any[]>([]);
  public loggedUser: Manager;
  public skillFilterValue: string;
  public skills: Skill[];
  public employees$: Observable<Employee[]>;
  public managers$: Observable<Manager[]>;
  private filterBySkillClickValue$: Observable<string>;
  private search$ = new Subject<string>();
  private employeesSource: any[];
  private subordinatesOnly: boolean = false;
  private filteredEmployees = new BehaviorSubject<any[]>([]);
  private recentlyFiltered = new BehaviorSubject<any[]>([]);
  private filterBySkillClickValue: string;

  public user = {
    name: '',
    position: '',
    email: '',
    password: '',
    isManager: false,
    isAdmin: false,
    availability: 8,
    manager: null,
    skills: [],
  };

  constructor(
    private fns: AngularFireFunctions,
    private modalService: NgbModal,
    private usersService: UsersService,
    private employeesService: EmployeesService,
    private authService: AuthService,
    private skillsService: SkillsService,
    private managersService: ManagersService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.employees$ = this.employeesService.getAllEmployees('');
    this.managers$ = this.managersService.getAllManagers();
    this.filterBySkillClickValue$ = this.employeesService.getSkillsFilterValue();

    combineLatest([
      this.employees$,
      this.managers$,
      this.filterBySkillClickValue$,
    ]).subscribe(([employees, managers, skillId]: any[]) => {
      this.employees.next(
        [...employees, ...managers].sort((a, b) => a.name.localeCompare(b.name))
      );
      this.employeesSource = [...employees, ...managers].sort((a, b) =>
        a.name.localeCompare(b.name)
      );

      this.filterBySkillClickValue = skillId;

      this.filteredEmployees.next(this.employeesSource);
    });

    this.authService.loadUser();
    this.authService.getLoggedUser().subscribe((manager) => {
      this.loggedUser = manager;
    });

    this.skillsService.getAllSkills().subscribe((skills: Skill[]) => {
      this.skills = skills;
    });

    this.search$.pipe(debounceTime(100)).subscribe((keyword: string) => {
      this.recentlyFiltered.next(this.employeesSource);
      this.recentlyFiltered.next(
        this.filteredEmployees.value
          .filter(
            (employee: any) =>
              employee.name.toUpperCase().includes(keyword) ||
              employee.email.toUpperCase().includes(keyword) ||
              employee.position.toUpperCase().includes(keyword)
          )
          .sort((a, b) => a.name.localeCompare(b.name))
      );

      this.employees.next([...this.recentlyFiltered.value]);

    });

    this.onSkillFilter();
  }

  ngAfterViewInit() {
    (this.skillSelect as any).nativeElement.selectedIndex = this.skills?.findIndex((sk: Skill) => sk.id === this.skillFilterValue) + 2;
  }

  public createUser() {
    const createUserFn = this.fns.httpsCallable('createUser');
    createUserFn(this.userData).subscribe((data) => {
      // console.log(data)
    });
  }

  public onCreateUser() {
    const ref = this.modalService.open(CreateEmployeeDialogComponent, {
      centered: true,
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
    });

    ref.result
      .then((user: CreateUser) => {
        if (user.isManager) {
          if (this.employees.value.filter(employee => employee.isManager).every(manager => manager.email !== user.email)) {
            const createUserFn = this.fns.httpsCallable('createUser');
            createUserFn(user).subscribe((data: ReturnUserCredentials) => {
              this.usersService.createManager(user, data.uid);
            });
          } else {
            this.toastr.error('Manager with this email already exists.');
          }
        } else {
          if (this.employees.value.every((employee: Employee) => employee.email !== user.email)) {
            this.usersService.createEmployee(user);
          } else {
            this.toastr.error('Employee with this email already exists.');
          }
        }
      })
      .catch((e: Error) => {
        if (e.message !== 'dismissed') {
          throw e;
        }
      });
  }

  public onShowSubordinates(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.subordinatesOnly = true;
      this.filterEmployees();
    } else {
      this.subordinatesOnly = false;
      this.filterEmployees();
    }
  }

  public onSearch(event: KeyboardEvent) {
    const filterValue: string = (event.target as HTMLInputElement).value
      .trim()
      .toUpperCase();

    this.search$.next(filterValue);
  }

  public onSkillFilter(event?: Event) {
    if (event) {
      this.skillFilterValue = (event.target as HTMLInputElement).value;
    } else {
      this.skillFilterValue = this.filterBySkillClickValue;
    }

    this.filterEmployees();
  }

  public filterEmployees() {
    if (this.subordinatesOnly) {
      this.filteredEmployees.next(
        this.employeesSource.filter(
          (employee) => employee.manager.id === this.loggedUser.id
        )
      );
    } else {
      this.filteredEmployees.next([...this.employeesSource]);
    }

    if (this.skillFilterValue) {
      if (this.skillFilterValue === 'managers') {
        this.filteredEmployees.next(
          this.filteredEmployees.value.filter((employee) => employee.isManager)
        );
      } else {
        this.filteredEmployees.next(
          this.filteredEmployees.value.filter((employee) =>
            employee.skillIds?.includes(this.skillFilterValue)
          )
        );
      }
    }
    this.employees.next([...this.filteredEmployees.value]);
  }

  ngOnDestroy() {
    this.employeesService.setSkillsFilterValue('');
  }
}
