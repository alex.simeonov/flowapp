import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { EmployeesComponent } from './employees.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CreateEmployeeDialogComponent } from './create-employee-dialog/create-employee-dialog.component';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { EmployeeCardComponent } from './employee-card/employee-card.component';
import { EmployeeViewComponent } from './employee-view/employee-view.component';
import { SkillsSelectComponent } from './create-employee-dialog/skills-select/skills-select.component';
import { WorkingProjectsComponent } from './employee-view/working-projects/working-projects.component';
import { EmployeeDetailsComponent } from './employee-view/employee-details/employee-details.component';
import { SingleWorkingProjectComponent } from './employee-view/working-projects/single-working-project/single-working-project.component';
import { EditEmployeeComponent } from './edit-employee/edit-employee.component';
import { EditEmployeeSkillsComponent } from './edit-employee/edit-employee-skills/edit-employee-skills.component';

// const routes: Routes = [
//   {
//     path: '',
//     component: EmployeesComponent
//   },
//   {
//     path: ':id',
//     component: EmployeeViewComponent
//   },
// ];

@NgModule({
  declarations: [
    EmployeesComponent,
    CreateEmployeeDialogComponent,
    EmployeeCardComponent,
    EmployeeViewComponent,
    SkillsSelectComponent,
    WorkingProjectsComponent,
    EmployeeDetailsComponent,
    SingleWorkingProjectComponent,
    EditEmployeeComponent,
    EditEmployeeSkillsComponent,
  ],
  imports: [
    SharedModule,
    FormsModule,
    // RouterModule.forChild(routes),
    NgBootstrapFormValidationModule,
  ],
  exports: [RouterModule],
  entryComponents: [EditEmployeeComponent]
})
export class EmployeesModule {}
