import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { Manager } from '../../../data/models/manager.model';
import { Skill } from '../../../data/models/skill.model';
import { Subject, of, BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, take, takeWhile, switchMap } from 'rxjs/operators';
import { EmployeesService } from '../../../data/services/employees.service';
import { Employee } from '../../../data/models/employee.model';
import { SkillsService } from '../../../data/services/skills.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit, AfterViewInit {
  @Input() public user: Manager;
  public form: FormGroup;
  public completionDays = new BehaviorSubject<string>('');
  public planningStatus = new BehaviorSubject<string>('');
  public allEmployees$: BehaviorSubject<{ [key: string]: Employee }> = new BehaviorSubject({});
  public allSkills$: BehaviorSubject<Skill[]> = new BehaviorSubject([]);

  constructor(
    private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private employeesService: EmployeesService,
    private skillsService: SkillsService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      skillHours: this.fb.array([]),
      employeeHours: this.fb.array([]),
    });

    // get all employees and map ids to object keys
    this.employeesService.getAllEmployees(null)
      .pipe(
        takeWhile((employees: Employee[]) => employees.length === 0, true),
        switchMap((employees: Employee[]) => {
          if (employees.length > 0) {
            this.allEmployees$.next(employees.reduce((acc, employee) => {
              acc[employee.id] = employee;

              return acc;
            }, {}));

            return this.skillsService.getAllSkills();
          } else {
            return of(null);
          }
        })
      )
      .subscribe(
        (data) => {
          // map all employees in skills to their availability
          if (data) {
            this.allSkills$.next(data.map(
              (skill: Skill) => ({
                ...skill,
                employees: skill.employees?.map((employee: Employee) => ({
                  ...employee,
                  ref: this.allEmployees$.value[employee.id]
                }))
              })
            ));
          }
        }
      );





    this.getCompletionInfo();
  }

  ngAfterViewInit(): void {
    const managementTimeCtrl = this.form.get('managementTime');
    managementTimeCtrl.setValidators([
      Validators.required,
      Validators.min(0),
      Validators.max(8),
      this.managementTimeValidator(this.user.availability)
    ]);
    setTimeout(() => { managementTimeCtrl.updateValueAndValidity(); }, 0);
  }

  public createProject(): void {
    if (this.planningStatus.value === 'On target') {
      this.form.value.planningStatus = true;
    } else {
      this.form.value.planningStatus = false;
    }

    this.form.value.daysToComplete = +this.completionDays.value;

    this.activeModal.close(this.form.value);
  }

  private managementTimeValidator(availability: number) {
    return (control: AbstractControl): { availability: number } | null => {
      if (availability - control.value < 0) {
        return { availability };
      }
      return null;
    };
  }

  public getCompletionInfo() {
    this.form.valueChanges
      .pipe(debounceTime(100))
      .subscribe(() => {
        const completionDays = this.getMaxDays();
        if (completionDays === Number.POSITIVE_INFINITY) {
          this.completionDays.next(`&infin;`);
        } else {
          this.completionDays.next(completionDays.toString());
        }

        if (completionDays === -1) {
          this.planningStatus.next('N/A');
        } else if (completionDays <= this.form.get('target').value) {
          this.planningStatus.next('On target');
        } else {
          this.planningStatus.next('Late');
        }
      });
  }

  public getMaxDays(): number {
    let maxDays = -1;
    if (!this.form.get('target').value) {
      return maxDays;
    }
    const skillHours = this.form.get('skillHours').value;
    const employeeHours = this.form.get('employeeHours').value;

    skillHours.forEach((skill: any) => {
      const hours = employeeHours
        .reduce((acc: number, hour: any) => {
          if (hour.skillId === skill.id) {
            return acc + hour.time;
          } else {
            return acc;
          }
        }, 0);

      const daysToComplete: number = Math.ceil(skill.time / hours);

      if (maxDays < daysToComplete) {
        maxDays = daysToComplete;
      }
    });

    return maxDays;
  }
}
