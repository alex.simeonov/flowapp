import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormGroupDirective, Validators, AbstractControl } from '@angular/forms';
import { Employee } from '../../../../data/models/employee.model';
import { ErrorMessage } from 'ng-bootstrap-form-validation';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'app-employee-hours',
  templateUrl: './employee-hours.component.html',
  styles: []
})
export class EmployeeHoursComponent implements OnInit, OnDestroy {
  @Input() public allEmployees$: BehaviorSubject<{ [key: string]: Employee }>;
  @Input() public index: number;
  @Input() public employee: FormGroup;
  @Output() public deselect: EventEmitter<Employee> = new EventEmitter();
  private sub: Subscription = new Subscription();

  constructor(private fgd: FormGroupDirective) { }

  customErrorMessages: ErrorMessage[] = [
    {
      error: 'required',
      format: (label: string, error: any) => `Employee hours are required`
    }, {
      error: 'min',
      format: (label: string, error: any) => `Employee hours must be no less than ${error.min}`
    }, {
      error: 'max',
      format: (label: string, error: any) => {

        if (error.max < 1) {
          return 'Employee is not available, please reallocate his hours';
        }
        return `Employee only has ${error.max} hours available`;
      }
    }
  ];

  ngOnInit(): void {
    const form = this.fgd.form.parent.parent;
    const employeeHours = form.get('employeeHours');

    this.allEmployees$.value[this.employee.value.id].usedAvailability = {};

    this.sub.add(this.employee.valueChanges.pipe(
      debounceTime(100),
      distinctUntilChanged()
    ).subscribe(
      (employeeHour) => {
        const employee = this.allEmployees$.value[employeeHour.id];

        if (!employee.usedAvailability) {
          employee.usedAvailability = { [employeeHour.skillId]: employeeHour.time };
        }

        employee.usedAvailability[employeeHour.skillId] = employeeHour.time;
      }
    ));

    // dynamic validators for employee hours
    this.sub.add(employeeHours.valueChanges
      .pipe(debounceTime(100), distinctUntilChanged())
      .subscribe(() => {
        this.employee.get('time').clearValidators();
        this.employee.get('time').setValidators([
          Validators.required,
          Validators.min(1),
          Validators.max(this.remainingHours())
        ]);
        this.employee.get('time').updateValueAndValidity({ onlySelf: false, emitEvent: false });
      }));
  }

  private remainingHours() {
    if (this.employee.value.ref.usedAvailability) {
      const used = Object.entries(this.employee.value.ref.usedAvailability).reduce((acc, entry: any) => {
        if (entry[0] !== this.employee.value.skillId) {
          return acc + entry[1];
        } else {
          return acc;
        }
      }, 0);

      const remaining = this.employee.value.ref.availability - used;

      if (remaining < 0) {
        return 0;
      } else if (remaining > this.employee.value.ref.usedAvailability) {
        return this.employee.value.ref.usedAvailability;
      } else {
        return remaining;
      }
    } else {
      return this.employee.value.ref.availability;
    }
  }

  public OnItemDeselect(): void {
    this.deselect.emit(this.employee.value);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
