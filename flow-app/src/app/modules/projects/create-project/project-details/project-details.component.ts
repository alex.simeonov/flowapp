import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, FormBuilder, Validators } from '@angular/forms';
import { ErrorMessage } from 'ng-bootstrap-form-validation';
import { Manager } from '../../../../data/models/manager.model';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styles: []
})
export class ProjectDetailsComponent implements OnInit {
  @Input() public user: Manager;
  public details: FormGroup;

  constructor(private fb: FormBuilder, private fgd: FormGroupDirective) { }

  errorProjectTarget: ErrorMessage[] = [
    {
      error: 'required',
      format: (label: string, error: any) => `Project target days are required`
    }, {
      error: 'min',
      format: (label: string, error: any) => `Project target days must be no less than ${error.min}`
    }, {
      error: 'max',
      format: (label: string, error: any) => `Project target days must be no greater than ${error.max}`
    }
  ];
  errorManagementTime: ErrorMessage[] = [
    {
      error: 'required',
      format: (label: string, error: any) => `Project management hours are required`
    }, {
      error: 'min',
      format: (label: string, error: any) => `Project management hours must be no less than ${error.min}`
    }, {
      error: 'max',
      format: (label: string, error: any) => `Project management hours must be no greater than ${error.max}`
    }, {
      error: 'availability',
      format: (label: string, error: number) => `Project management hours must be no greater than your current availability of ${error} hours/day.`
    }
  ];

  ngOnInit(): void {
    this.details = this.fgd.form;
    this.details.addControl('name', this.fb.control('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(64)
    ]));
    this.details.addControl('description', this.fb.control('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20000)
    ]));
    this.details.addControl('target', this.fb.control('', [
      Validators.required,
      Validators.min(0),
      Validators.max(3650)
    ]));
    this.details.addControl('managementTime', this.fb.control(''));
  }
}
