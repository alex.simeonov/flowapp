import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormGroupDirective, FormArray, Validators } from '@angular/forms';
import { Subject, BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeWhile } from 'rxjs/operators';
import { DropdownSettings } from 'angular2-multiselect-dropdown/lib/multiselect.interface';
import { Skill } from '../../../../data/models/skill.model';
import { Employee } from '../../../../data/models/employee.model';
import { ErrorMessage } from 'ng-bootstrap-form-validation';

@Component({
  selector: 'app-edit-project-skills',
  templateUrl: './project-skills.component.html',
  styles: []
})
export class EditProjectSkillsComponent implements OnInit {
  @Input() public currentSkillsEmployees: BehaviorSubject<any[]>;
  @Input() public currentSkills: BehaviorSubject<Skill[]>;
  @Input() public allSkills$: BehaviorSubject<Skill[]>;
  @Input() public allEmployees$: BehaviorSubject<{ [key: string]: Employee }>;
  @Input() public projectId: string;

  public projectSkills: FormGroup;
  private allSkills: Skill[];

  @ViewChild('searchInput') public searchInput: ElementRef;
  private search$ = new Subject<string>();

  public dropdownSettings: Partial<DropdownSettings> = {};
  public dropdownList: Skill[] = [];
  public selectedItems: any[] = [];

  constructor(
    private fb: FormBuilder,
    private fgd: FormGroupDirective,
    private cd: ChangeDetectorRef
  ) { }

  public customErrorMessages: ErrorMessage[] = [
    {
      error: 'required',
      format: (label: string, error: any) => `Project skills are required`
    }
  ];

  ngOnInit(): void {
    this.projectSkills = this.fgd.form;
    this.projectSkills.addControl('skills', this.fb.control('', [
      Validators.required,
    ]));

    this.allSkills$
      .pipe(takeWhile((skills: Skill[]) => skills.length === 0, true))
      .subscribe(
        (skills: Skill[]) => {
          this.allSkills = [...skills];
          this.dropdownList = [...skills];

          const currentSkillIds = this.currentSkills.value.map((skill: Skill) => skill.id);

          const selectedSkills = skills.reduce((acc: Skill[], skill: Skill) => {
            if (currentSkillIds.includes(skill.id)) {
              const idx = currentSkillIds.indexOf(skill.id);
              skill.time = this.currentSkills.value[idx].time;
              skill.progress = this.currentSkills.value[idx].progress;
              acc.push(skill);
            }

            return acc;
          }, []);

          this.selectedItems = selectedSkills;
          this.selectedItems.forEach((skill: Skill) => this.onItemSelect(skill));
        }
      );

    this.dropdownSettings = {
      enableSearchFilter: true,
      enableCheckAll: false,
      text: 'Select skills',
      noDataLabel: 'No such skill',
      labelKey: 'name',
      primaryKey: 'id',
      // searchBy: ['name'],
    };

    this.search$.pipe(
      debounceTime(100),
      distinctUntilChanged()
    ).subscribe((filterValue: string) => {
      this.dropdownList = this.allSkills.filter((item: any) => item.name.toUpperCase().includes(filterValue));
      this.cd.detectChanges();
    });
  }

  public get hours(): FormArray {
    return this.fgd.form.get('skillHours') as FormArray;
  }

  public addSkillHours(skill: Skill): void {

    this.hours.push(
      this.fb.group({
        id: skill.id,
        name: skill.name,
        employees: this.fb.array(skill.employees),
        time: this.fb.control(skill.time, [
          Validators.required,
          Validators.min(skill.progress),
          Validators.max(3650)
        ]),
        progress: skill.progress,
      })
    );
  }

  public removeSkillHours(skill: Skill): void {
    const idx: number = this.hours.value?.map((item: any) => item.id).indexOf(skill.id);
    this.hours.removeAt(idx);

    const skillIds: string[] = this.fgd.form.get('employeeHours').value?.map((item: any) => item.skillId);
    const empIds: string[] = this.fgd.form.get('employeeHours').value?.map((item: any) => item.id);

    const idxToRemove: number[] = [];

    skillIds.forEach((id: string, index: number) => {
      if (id === skill.id) {
        idxToRemove.push(index);
      }
    });

    idxToRemove.reverse().forEach((index: number) =>
      (this.fgd.form.get('employeeHours') as FormArray).removeAt(index)
    );

    empIds.forEach((id: string) => this.allEmployees$.value[id].usedAvailability[skill.id] = 0);
  }

  public deselectSkill(skill: Skill): void {
    this.removeSkillHours(skill);

    const filteredSkills = this.projectSkills.value.skills.filter((item: any) => item.id !== skill.id);
    this.projectSkills.get('skills').setValue(filteredSkills);
  }

  public onSearch(event: KeyboardEvent): void {
    const filterValue: string = (event.target as HTMLInputElement).value.trim().toUpperCase();
    this.search$.next(filterValue);
  }
  public onItemSelect(skill: Skill): void {
    this.addSkillHours(skill);
  }
  public OnItemDeselect(skill: Skill): void {
    this.removeSkillHours(skill);
  }
  public onDeselectAll(): void {
    this.projectSkills.value.skills = [];
    this.hours.clear();
  }

  public onOpen(): void {
    setTimeout(() => {
      this.searchInput.nativeElement.focus();
    }, 0);
  }
}
