import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, Input, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { FormBuilder, FormArray, FormGroup, FormGroupDirective, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Employee } from '../../../../data/models/employee.model';
import { DropdownSettings } from 'angular2-multiselect-dropdown/lib/multiselect.interface';
import { SkillEmployee } from '../../../../data/models/skill-employee.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-skill-employees',
  templateUrl: './skill-employees.component.html',
  styles: []
})
export class EditSkillEmployeesComponent implements OnInit, AfterViewInit {
  public currentSkillEmployeeIds: string[];
  @Input() public currentSkillsEmployees: BehaviorSubject<any[]>;
  @Input() public allEmployees$: BehaviorSubject<{ [key: string]: Employee }>;
  @Input() public projectSkill: FormControl;
  @Input() public skillIndex: number;
  @Input() public projectId: string;

  @ViewChild('a2multiselect') public a2multiselect;
  @ViewChild('searchInput') public searchInput: ElementRef;
  private search$ = new Subject<string>();

  public employeeSkillHours: FormGroup;
  public employees: SkillEmployee[];

  public dropdownSettings: Partial<DropdownSettings> = {};
  public dropdownList: SkillEmployee[] = [];
  public selectedItems: SkillEmployee[] = [];

  public disableTooltip: boolean;

  constructor(
    private fb: FormBuilder,
    private fgd: FormGroupDirective,
    public activeModal: NgbActiveModal,
    private cd: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.currentSkillEmployeeIds = this.currentSkillsEmployees.value.map((skEmp: SkillEmployee) => skEmp.skillId + '_' + skEmp.id);

    this.disableTooltip = this.projectSkill.value.time > 0;

    this.employeeSkillHours = (this.fgd.form.parent as FormArray).at(this.skillIndex) as FormGroup;
    this.employeeSkillHours.addControl('selectedEmployees', this.fb.control(''));

    const selectedEmps = this.employeeSkillHours.get('employees').value.reduce((acc: SkillEmployee[], emp: SkillEmployee) => {
      const skillEmployee = this.currentSkillsEmployees.value
        .find((skEmp: SkillEmployee) => (skEmp.id === emp.id && skEmp.skillId === this.projectSkill.value.id));

      if (skillEmployee) {
        emp.time = skillEmployee.time;
        emp.skillId = skillEmployee.skillId;
        emp.skillName = skillEmployee.skillName;

        if (!emp.ref.projAvailability) {
          emp.ref.projAvailability = {};
        }
        if (!emp.ref.projAvailability[this.projectId]) {
          emp.ref.projAvailability[this.projectId] = {};
        }

        emp.ref.projAvailability[this.projectId][this.projectSkill.value.id] = skillEmployee.time;
        acc.push(emp);
      }

      return acc;
    }, []);

    const emps = this.employeeSkillHours.get('employees').value
      .filter((emp: Employee) => emp?.ref?.availability >= 1 || emp?.ref?.projAvailability);

    this.employees = emps;
    this.dropdownList = [...this.employees];

    this.selectedItems = selectedEmps;
    this.selectedItems.forEach((skillEmp: SkillEmployee) => this.onItemSelect(skillEmp));

    this.dropdownSettings = {
      enableSearchFilter: true,
      enableCheckAll: false,
      text: `Select employees`,
      noDataLabel: 'No employees found',
      labelKey: 'name',
      primaryKey: 'id',
      disabled: this.projectSkill.value.time <= 0,
      // searchBy: ['name'],
    };

    this.search$.pipe(
      debounceTime(100),
      distinctUntilChanged()
    ).subscribe((filterValue: string) => {

      this.dropdownList = this.employees.filter((item) => item.name.toUpperCase().includes(filterValue));
      this.cd.detectChanges();
    });

  }

  ngAfterViewInit() {
    this.employeeSkillHours.valueChanges.subscribe((data: any) => {
      if (data.time <= 0 || data.time > 3650) {
        this.a2multiselect.settings.disabled = true;
        this.disableTooltip = false;
        this.a2multiselect.selectedItems = [];
        this.onDeselectAll();
      } else {
        this.disableTooltip = true;
        this.a2multiselect.settings.disabled = false;
      }
    });
  }

  public get employeeHours(): FormArray {
    return this.fgd.form.parent.parent.get('employeeHours') as FormArray;
  }
  public addEmployeeHours(employee: SkillEmployee): void {
    this.employeeHours.push(
      this.fb.group({
        skillName: this.projectSkill.value.name,
        skillId: this.projectSkill.value.id,
        ...employee,
        time: this.fb.control(employee.time)
      })
    );
  }
  public removeEmployeeHours(employee: SkillEmployee): void {
    employee.time = null;

    employee.ref.usedAvailability[this.projectSkill.value.id] = 0;

    const idx: number = this.employeeHours.value
      .map((item: any) => item.skillId + '_' + item.id).indexOf(employee.skillId + '_' + employee.id);

    this.employeeHours.removeAt(idx);
  }

  public deselectEmployee(employee: SkillEmployee): void {
    this.removeEmployeeHours(employee);

    const newEmployees = this.employeeSkillHours.get('selectedEmployees').value.filter((item: SkillEmployee) => item.id !== employee.id);
    this.employeeSkillHours.get('selectedEmployees').setValue(newEmployees);
  }

  public onSearch(event: KeyboardEvent): void {
    const filterValue: string = (event.target as HTMLInputElement).value.trim().toUpperCase();
    this.search$.next(filterValue);
  }
  public onItemSelect(item: SkillEmployee): void {
    this.addEmployeeHours(item);
  }
  public OnItemDeSelect(item: SkillEmployee): void {
    this.removeEmployeeHours(item);
  }
  public onDeselectAll(): void {
    const indexes: number[] = [];

    this.employeeHours.value.forEach(
      (value: any, index: number) => {
        if (value.skillId === this.projectSkill.value.id) {
          indexes.push(index);
        }
      });

    indexes.reverse().forEach((index: number) => this.employeeHours.removeAt(index));
  }

  public onOpen(): void {
    setTimeout(() => {
      this.searchInput.nativeElement.focus();
    }, 0);
  }
}
