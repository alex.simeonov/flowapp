import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Skill } from '../../../../data/models/skill.model';
import { ErrorMessage } from 'ng-bootstrap-form-validation';
import { BehaviorSubject } from 'rxjs';
import { Employee } from '../../../../data/models/employee.model';

@Component({
  selector: 'app-edit-skill-hours',
  templateUrl: './skill-hours.component.html',
  styles: []
})
export class EditSkillHoursComponent implements OnInit {
  @Input() public currentSkillsEmployees: BehaviorSubject<any[]>;
  @Input() public allEmployees$: BehaviorSubject<{ [key: string]: Employee }>;
  @Input() public index: number;
  @Input() public skill: FormGroup;
  @Output() public deselect: EventEmitter<Skill> = new EventEmitter();
  @Input() public projectId: string;

  constructor() { }

  errorSkillHours: ErrorMessage[] = [
    {
      error: 'required',
      format: (label: string, error: any) => `Skill hours are required`
    }, {
      error: 'min',
      format: (label: string, error: any) => `Skill hours must be no less than current progress hours: ${error.min}`
    }, {
      error: 'max',
      format: (label: string, error: any) => `Skill hours must be no greater than ${error.max}`
    }
  ];

  ngOnInit(): void { }

  public OnItemDeselect(): void {
    this.deselect.emit(this.skill.value);
  }
}
