import { Skill } from './../../../data/models/skill.model';
import { take, takeLast } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { Project } from './../../../data/models/project.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectsService } from '../../../../app/data/services/projects.service';
import { BehaviorSubject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditProjectComponent } from '../edit-project/edit-project.component';
import { AuthService } from '../../../data/services/auth.service';
import { Manager } from '../../../data/models/manager.model';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.scss'],
})
export class ProjectViewComponent implements OnInit, OnDestroy {
  public projectId: string;
  public managerId: string;
  private user: Manager;
  public currentProject = new BehaviorSubject<Project>(null);
  public skillEmployee = new BehaviorSubject<any[]>([]);
  public skills = new BehaviorSubject<any[]>([]);
  public startDate: Date;
  public daysNeeded: string;
  public disableTooltip = true;

  constructor(
    private route: ActivatedRoute,
    private projectsService: ProjectsService,
    private auth: AngularFireAuth,
    private authService: AuthService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.projectId = this.route.snapshot.params.id;
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.managerId = user.uid;
      }
    });

    this.authService.getLoggedUser().subscribe((user: Manager) => {
      this.user = user;
    });

    this.projectsService
      .getProject(this.projectId)
      .subscribe(([project, skillEmployee, skills]) => {
        if (project) {
          this.currentProject.next(project);
          this.startDate = (project.startingDate as firebase.firestore.Timestamp).toDate();
          if (project.progressStatus) {
            this.disableTooltip = false;
          } else {
            this.disableTooltip = true;
          }

          if (project.daysToComplete === -1) {
            this.daysNeeded = `&infin;`;
          } else {
            this.daysNeeded = String(project.daysToComplete);
          }
        }
        if (skillEmployee) {
          this.skillEmployee.next(skillEmployee);
        }
        if (skills) {
          this.skills.next(skills);
        }
      });
  }

  public stopProject() {
    this.projectsService.stopProject();
  }

  public editProject() {
    const modal = this.modalService.open(EditProjectComponent, {
      centered: true,
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      scrollable: true,
    });

    // pass data to modal
    modal.componentInstance.user = this.user;
    modal.componentInstance.currentProject = this.currentProject;
    modal.componentInstance.currentSkillsEmployees = this.skillEmployee;
    modal.componentInstance.currentSkills = this.skills;
    modal.componentInstance.projectId = this.projectId;


    modal.result
      .then((proj: any) => {
        const newProj = {
          name: proj.name,
          description: proj.description,
          managementTime: proj.managementTime,
          target: proj.target,
          startingDate: this.currentProject.value.startingDate,
          manager: this.currentProject.value.manager,
          progressStatus: false,
          planningStatus: proj.planningStatus,
          elapsedDays: this.currentProject.value.elapsedDays,
          daysToComplete: (isNaN(proj.daysToComplete) || proj.daysToComplete < 1) ? -1 : proj.daysToComplete
        };

        const employeeHours = proj.employeeHours;

        const skillHours = proj.skillHours.map((skill: any) =>
          ({
            id: skill.id,
            name: skill.name,
            time: skill.time,
            employeeIds: skill.selectedEmployees ? skill.selectedEmployees.map((employee: any) => employee.id) : [],
            progress: skill.progress,
          }));

        this.projectsService.editProject(newProj, employeeHours, skillHours);
      })
      .catch((msg: string) => {
        // modal dismissed
      });
  }

  ngOnDestroy() {
    this.currentProject.next(null);
    this.skillEmployee.next(null);
    this.skills.next(null);
    this.disableTooltip = false;
  }
}
