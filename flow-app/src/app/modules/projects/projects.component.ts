import { ProjectSkillsComponent } from './create-project/project-skills/project-skills.component';
import { BehaviorSubject, Observable } from 'rxjs';
import { SkillsService } from '../../../app/data/services/skills.service';
import { Project } from './../../data/models/project.model';
import { ProjectsService } from './../../data/services/projects.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateProjectComponent } from './create-project/create-project.component';
import { AuthService } from '../../data/services/auth.service';
import { Manager } from '../../data/models/manager.model';
import { Skill } from '../../../app/data/models/skill.model';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit {
  public projects = new BehaviorSubject<Project[]>([]);
  private user: Manager;
  public skills: Skill[];
  public ownProjectsOnly = false;
  public search$ = new BehaviorSubject<string>('');
  public skillFilterValue: string;
  public statusFilterValue: string;
  public projects$: Observable<Project[]>;
  private projectsSource: Project[];
  private filteredProjects = new BehaviorSubject<Project[]>([]);
  private recentlyFiltered = new BehaviorSubject<Project[]>([]);

  constructor(
    private projectsService: ProjectsService,
    private modalService: NgbModal,
    private authService: AuthService,
    private skillsService: SkillsService
  ) { }

  ngOnInit(): void {
    this.projects$ = this.projectsService.getAllProjects();
    this.projects$.subscribe((projects: Project[]) => {
      this.projects.next(projects.sort((a, b) => +b.startingDate - +a.startingDate));
      this.projectsSource = projects.sort((a, b) => +b.startingDate - +a.startingDate);
      this.filteredProjects.next(this.projectsSource);
    });

    this.authService.getLoggedUser().subscribe((user: Manager) => {
      this.user = user;
    });

    this.skillsService.getAllSkills().subscribe((skills: Skill[]) => {
      this.skills = skills;
    });

    this.search$.pipe(debounceTime(100)).subscribe((keyword: string) => {
      this.recentlyFiltered.next(this.projectsSource);
      this.recentlyFiltered.next(
        this.filteredProjects.value
          .filter(
            (employee: any) =>
              employee.name.toUpperCase().includes(keyword) ||
              employee.description.toUpperCase().includes(keyword) ||
              employee.manager.name.toUpperCase().includes(keyword)
          )
          .sort((a, b) => +b.startingDate - +a.startingDate)
      );

      this.projects.next([...this.recentlyFiltered.value]);
    });
  }

  public onShowOwn(event: Event) {
    if ((event.target as HTMLInputElement).checked) {
      this.ownProjectsOnly = true;
      this.filterProjects();
    } else {
      this.ownProjectsOnly = false;
      this.filterProjects();
    }
  }

  public onSearch(event: Event) {
    const filterValue: string = (event.target as HTMLInputElement).value
      .trim()
      .toUpperCase();

    this.search$.next(filterValue);
  }

  public onSkillFilter(event: Event) {
    this.skillFilterValue = (event.target as HTMLInputElement).value;
    this.filterProjects();
  }

  public onFilterByStatus(event: Event) {
    this.statusFilterValue = (event.target as HTMLInputElement).value;
    this.filterProjects();
  }

  public createProject() {
    const modal = this.modalService.open(CreateProjectComponent, {
      centered: true,
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      scrollable: true,
    });

    // pass data to modal
    modal.componentInstance.user = this.user;

    modal.result
      .then((proj: any) => {
        const newProj = {
          name: proj.name,
          description: proj.description,
          managementTime: proj.managementTime,
          target: proj.target,
          startingDate: '',
          manager: {
            id: this.user.id,
            name: this.user.name,
          },
          progressStatus: false,
          planningStatus: proj.planningStatus,
          elapsedDays: 0,
          daysToComplete: (isNaN(proj.daysToComplete) || proj.daysToComplete < 1) ? -1 : proj.daysToComplete
        };

        const employeeHours = proj.employeeHours;

        const skillHours = proj.skillHours.map((skill: any) =>
          ({
            id: skill.id,
            name: skill.name,
            time: skill.time,
            employeeIds: skill.selectedEmployees ? skill.selectedEmployees.map((employee: any) => employee.id) : [],
          }));

        this.projectsService.createProject(newProj, employeeHours, skillHours);
      })
      .catch((msg: string) => {
        // modal dismissed
      });
  }

  public filterProjects() {
    if (this.ownProjectsOnly) {
      this.filteredProjects.next(
        this.projectsSource.filter(
          (project) => project.manager.id === this.user.id
        )
      );
    } else {
      this.filteredProjects.next([...this.projectsSource]);
    }

    if (this.skillFilterValue) {
      this.filteredProjects.next(
        this.filteredProjects.value.filter((project) =>
          project.skillIds?.includes(this.skillFilterValue)
        )
      );
    }

    if (this.statusFilterValue === 'finished') {
      this.filteredProjects.next(this.filteredProjects.value.filter(
        (project: Project) => project.progressStatus
      ));
    } else if (this.statusFilterValue === 'in-progress') {
      this.filteredProjects.next(this.filteredProjects.value.filter(
        (project: Project) => !project.progressStatus
      ));
    }

    this.projects.next([...this.filteredProjects.value]);
  }
}
