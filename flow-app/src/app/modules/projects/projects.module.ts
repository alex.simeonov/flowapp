import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProjectsComponent } from './projects.component';
import { ProjectCardComponent } from './project-card/project-card.component';
import { SharedModule } from '../../shared/shared.module';
import { ProjectViewComponent } from './project-view/project-view.component';
import { CreateProjectComponent } from './create-project/create-project.component';
import { SkillEmployeesComponent } from './create-project/skill-employees/skill-employees.component';
import { ProjectDetailsComponent } from './create-project/project-details/project-details.component';
import { ProjectSkillsComponent } from './create-project/project-skills/project-skills.component';
import { SkillHoursComponent } from './create-project/skill-hours/skill-hours.component';
import { EmployeeHoursComponent } from './create-project/employee-hours/employee-hours.component';
import { EditProjectComponent } from './edit-project/edit-project.component';
import { EditSkillEmployeesComponent } from './edit-project/skill-employees/skill-employees.component';
import { EditProjectSkillsComponent } from './edit-project/project-skills/project-skills.component';
import { EditSkillHoursComponent } from './edit-project/skill-hours/skill-hours.component';
import { EditEmployeeHoursComponent } from './edit-project/employee-hours/employee-hours.component';
import { EditProjectDetailsComponent } from './edit-project/project-details/project-details.component';


// const routes: Routes = [
//   {
//     path: '',
//     component: ProjectsComponent,
//   },
//   {
//     path: ':id',
//     component: ProjectViewComponent
//   }
// ];

@NgModule({
  declarations: [
    ProjectsComponent,
    ProjectCardComponent,
    ProjectViewComponent,
    CreateProjectComponent,
    SkillEmployeesComponent,
    ProjectDetailsComponent,
    ProjectSkillsComponent,
    SkillHoursComponent,
    EmployeeHoursComponent,
    EditProjectComponent,
    EditSkillEmployeesComponent,
    EditProjectDetailsComponent,
    EditProjectSkillsComponent,
    EditSkillHoursComponent,
    EditEmployeeHoursComponent,
  ],
  imports: [
    SharedModule,
    // RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  entryComponents: [CreateProjectComponent, EditProjectComponent]
})
export class ProjectsModule { }
