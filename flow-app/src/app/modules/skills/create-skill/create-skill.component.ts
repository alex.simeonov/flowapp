import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create-skill',
  templateUrl: './create-skill.component.html',
  styleUrls: ['./create-skill.component.scss'],
})
export class CreateSkillComponent implements OnInit {
  public name: string;

  createSkill: FormGroup = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
    ]),
  });

  constructor(public modal: NgbActiveModal) {}

  ngOnInit(): void {}

  create() {
    this.name = this.createSkill.value.name;
    this.modal.close(this.name);
  }
}
