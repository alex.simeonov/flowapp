import { EmployeesService } from './../../../data/services/employees.service';
import { Router } from '@angular/router';
import { Skill } from './../../../data/models/skill.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-skill-card',
  templateUrl: './skill-card.component.html',
  styleUrls: ['./skill-card.component.scss'],
})
export class SkillCardComponent implements OnInit {
  @Input() public singleSkill;

  constructor(
    private router: Router,
    private employeesService: EmployeesService
  ) {}

  ngOnInit(): void {}

  public onSkillSelect() {
    this.router.navigate(['/employees']);
    this.employeesService.setSkillsFilterValue(this.singleSkill.id);
  }
}
