import { createUser } from './../../../../functions/src/index';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SkillsService } from './../../data/services/skills.service';
import { SharedModule } from './../../shared/shared.module';
import { SkillsComponent } from './skills.component';
import { Routes, RouterModule } from '@angular/router';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { SkillCardComponent } from './skill-card/skill-card.component';
import { CreateSkillComponent } from './create-skill/create-skill.component';
import { of } from 'rxjs';

describe('SkillsComponent', () => {
  const routes: Routes = [];

  let fixture: ComponentFixture<SkillsComponent>;
  let component: SkillsComponent;
  let skillsService;
  let modalService;
  const skillsArr = [{ name: 'abc' }, { name: 'cba' }, { name: 'bca' }];

  beforeEach(async(() => {
    jest.clearAllMocks();

    skillsService = {
      getAllSkills() {
        /* empty */
        return of(skillsArr);
      },
      createSkill() {
        /* empty */
      },
    };

    modalService = {
      open() {
        return NgbModalRef;
      },
      httpsCallable() {
        /* empty */
      },
    };

    TestBed.configureTestingModule({
      declarations: [SkillsComponent, SkillCardComponent, CreateSkillComponent],
      imports: [SharedModule, RouterModule.forChild(routes)],
      providers: [SkillsService, NgbModal],
    })
      .overrideProvider(SkillsService, { useValue: skillsService })
      .overrideProvider(NgbModal, { useValue: modalService })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SkillsComponent);
        component = fixture.componentInstance;
        // (component as any).employeesSource = [];
        // (component as any).combineLatest = () => of(null);
      });
  }));

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('ngOnInit()', () => {
    it('should call skillsService.getAllSkills() once', () => {
      // Arrange
      const spy = jest.spyOn(skillsService, 'getAllSkills');

      // Act
      component.ngOnInit();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should return the correct value', () => {
      // Arrange
      const spy = jest
        .spyOn(skillsService, 'getAllSkills')
        .mockImplementation((skills) => of(skillsArr));
      const result = [{ name: 'abc' }, { name: 'bca' }, { name: 'cba' }];

      // Act & Assert
      skillsService
        .getAllSkills()
        .subscribe((data) => expect(result).toEqual(data));
    });
  });

  describe('createSkill', () => {
    it('should call modalService.open() once', () => {
      // Arrange
      const modalRef = modalService.open(CreateSkillComponent);
      const spy = jest.spyOn(modalService, 'open').mockReturnValue(modalRef);
      modalRef.result = new Promise(() => 'name');

      // Act
      component.createSkill();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should call skillsService.createSkill() once with name', () => {
      // Arrange
      const modalRef = modalService.open(CreateSkillComponent);
      const spy = jest.spyOn(modalService, 'open').mockReturnValue(modalRef);
      const spyFn = jest
        .spyOn(skillsService, 'createSkill')
        .mockReturnValue(modalRef);
      modalRef.result = new Promise(() => 'name');

      // Act & Assert
      component.createSkill();
      modalRef.result.then(() => expect(spyFn).toHaveBeenCalledWith('name'));
    });
  });
});
