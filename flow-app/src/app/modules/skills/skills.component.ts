import { ToastrService } from 'ngx-toastr';
import { createUser } from './../../../../functions/src/index';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateSkillComponent } from './create-skill/create-skill.component';
import { SkillsService } from './../../data/services/skills.service';
import { Skill } from './../../data/models/skill.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
})
export class SkillsComponent implements OnInit {
  public skills: Skill[];

  constructor(
    private skillsService: SkillsService,
    private modalService: NgbModal,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.skillsService.getAllSkills().subscribe((skills: Skill[]) => {
      this.skills = skills.sort((a, b) => a.name.localeCompare(b.name));
    });
  }

  createSkill() {
    const ref = this.modalService.open(CreateSkillComponent, {
      centered: true,
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
    });

    ref.result
      .then((name: string) => {
        if (name) {
          if (this.skills.every((skill: Skill) => skill.name !== name)) {
            this.skillsService.createSkill(name);
          } else {
            this.toastr.error('This skill already exists.');
          }
        }
      })
      .catch((e: Error) => {
        if (e.message !== 'dismissed') {
          throw e;
        }
      });
  }
}
