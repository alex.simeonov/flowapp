import { AdminGuard } from './../../core/guards/admin-guard.service';
import { SkillsService } from './../../data/services/skills.service';
import { SkillsComponent } from './skills.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../app/shared/shared.module';
import { SkillCardComponent } from './skill-card/skill-card.component';
import { CreateSkillComponent } from './create-skill/create-skill.component';

// const routes: Routes = [
//   {
//     path: '',
//     component: SkillsComponent,
//     canActivate: [AdminGuard]
//   },

// ];

@NgModule({
  declarations: [
    SkillsComponent,
    SkillCardComponent,
    CreateSkillComponent,
  ],
  imports: [
    SharedModule,
    // RouterModule.forChild(routes),
  ],
  providers: [SkillsService]
})
export class SkillsModule { }
