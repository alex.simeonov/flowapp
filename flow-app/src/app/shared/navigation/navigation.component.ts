import { AuthService } from '../../data/services/auth.service';
import { Manager } from './../../data/models/manager.model';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  @Input() loggedUser: Manager;

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.getLoggedUser().subscribe((user) => {
      this.loggedUser = user;
    });
  }

  logout() {
    this.authService.logout().then(() =>
      this.router.navigate(['auth'])
    );
  }
}
