import { NavigationComponent } from './navigation.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';



@NgModule({
  declarations: [NavigationComponent],
  imports: [
    SharedModule
  ],
  exports: [
    NavigationComponent
  ]
})
export class NavigationModule { }
