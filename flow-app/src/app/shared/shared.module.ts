import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ErrorPageComponent } from './error-page/error-page.component';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  exports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    AngularMultiSelectModule,
    NgbModule,
    NgBootstrapFormValidationModule
  ],
  declarations: [ErrorPageComponent]
})
export class SharedModule { }
